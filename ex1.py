# João Gabriel Camacho Presotto
# Trabalho 1 - Computação Inspirada pela Natureza
# Exercício 1

import functions
import numpy as np
import time
import random
from progressbar import ProgressBar

# *********************************************************
# Parâmetros
# *********************************************************
population_size = 24
PC = 0.76 # taxa de crossover
pm = 0.0 # probabilidade de mutação
l = 12 # tamanho de cada individuo	
target = "111101101111" # Valor alvo = '0' -> 11110110111

# *********************************************************
# Funções
# *********************************************************

# f[i] = l - hammingDistance[i]
def aptitude_usingHamming(l,hammingDist):
	return np.array([(l - hammingDist[i]) for i in range(hammingDist.size)])

# ************************************************************ #
#					   Algoritmo Genético 				   	   #
# ************************************************************ #	
# Parametros:
# PC = taxa de crossover [0, 1.0]
# pm = taxa de mutação [0, 1.0]
# target = bitstring do individuo alvo
# population = população que vai passar pelo algoritmo
# elistism = True ou False 
# selection = 'roulette' ou 'tournament'
# ************************************************************ #
def genetic_algorithm(PC, pm, target, population, elitism, selection):
	mean_aptitude_values = []
	best_aptitude_values = []
	optimal_individual = False
	num_iter = 0
	pop_size = population.shape[0]

	while not optimal_individual:	
		# Calculando Distância de Hamming
		h = functions.hammingDistance(target,population)

		# Calculando aptidão (f) da população
		f = aptitude_usingHamming(target.size,h)

		if elitism:
			# Encontra o melhor individuo da geração atual
			best_individual_index = np.argmax(f)
			# Salva o melhor indivíduo
			best_individual = population[best_individual_index,]	

		if selection == "roulette":
			# Seleção por roleta dos indivíduos
			P = functions.selectNewPopulationByRoulette(f,population)
		elif selection == "tournament":
			# Seleção por torneio dos individuos
			P = functions.selectNewPopulationByTournament(f,population)
		
		# Reprodução
		P = functions.reproduction(P,PC,pm,target.size)

		if elitism:
			# Adiciona o melhor individuo que havia sido salvo na última posição da população
			P[pop_size-1,] = best_individual

		# Avaliação
		new_h = functions.hammingDistance(target,P)
		new_f = functions.aptitude(target.size,new_h)
		
		if mean_aptitude_values == []:
			# Salva a aptidão do melhor indivíduo e a media da População inicial
			best_aptitude_values.append(f[np.argmax(f)])
			mean_aptitude_values.append(round(f.sum()/pop_size))
		
		# Salva a aptidão do melhor indivíduo e a media dos individuos
		best_aptitude_values.append(new_f[np.argmax(new_f)])
		mean_aptitude_values.append(round(new_f.sum()/pop_size))

		population = P

		# se um individuo atingiu a aptidao maxima, vai sair do laço
		if target.size in new_f:
			optimal_individual=True

		num_iter += 1

	mean_aptitude_values = np.array(mean_aptitude_values, dtype=int)
	best_aptitude_values = np.array(best_aptitude_values)

	return population, mean_aptitude_values, best_aptitude_values, num_iter, num_iter+1

# *********************************************************
def main():
	start = time.time()	

	global target
	# Convertendo valor alvo para bistring
	target = np.array(list(target), dtype=int)

	# Variaveis para gerar os gráficos
	exec_timeA,exec_timeB, exec_timeC, exec_timeD = ([] for i in range(4))
	num_ger_totalA,	num_ger_totalB, num_ger_totalC, num_ger_totalD = ([] for i in range(4))
	best_aptitude_meanA, best_aptitude_meanB, best_aptitude_meanC, best_aptitude_meanD = ([] for i in range(4))
	mean_aptitude_meanA, mean_aptitude_meanB, mean_aptitude_meanC, mean_aptitude_meanD = ([] for i in range(4))

	p_bar = ProgressBar()
	
	for i in p_bar(range(100)):
		# Inicializando população com indivíduos de tamanho 12 com valores aleatórios 0 ou 1
		init = np.random.randint(2	, size=population_size*l)
		population = np.reshape(init, (population_size,l))

		#print("pop inicial", population)

		# Algoritmos geneticos
		# Roleta
		startA = time.time()
		populationA, mean_aptitude_valuesA, best_aptitude_valuesA, num_iterA, num_generationsA = genetic_algorithm(PC,pm,target,population,elitism=False,selection="roulette")
		endA = time.time()

		# Roleta + Elitismo
		startB = time.time()
		populationB, mean_aptitude_valuesB, best_aptitude_valuesB, num_iterB, num_generationsB = genetic_algorithm(PC,pm,target,population,elitism=True,selection="roulette")
		endB = time.time()

		# Torneio
		startC = time.time()
		populationC, mean_aptitude_valuesC, best_aptitude_valuesC, num_iterC, num_generationsC = genetic_algorithm(PC,pm,target,population,elitism=False,selection="tournament")
		endC = time.time()

		#Torneio + Elitismo
		startD = time.time()
		populationD, mean_aptitude_valuesD, best_aptitude_valuesD, num_iterD, num_generationsD = genetic_algorithm(PC,pm,target,population,elitism=True,selection="tournament")
		endD = time.time()

		# Gráficos da Execução 50
		if i == 50:
			#Roleta
			functions.plot_aptitudes(best_aptitude_valuesA,mean_aptitude_valuesA,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/exec50Roleta.pdf",option="roleta; ")
			# Roleta + Elitismo
			functions.plot_aptitudes(best_aptitude_valuesB,mean_aptitude_valuesB,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/exec50Roleta+Elitismo.pdf",option="roleta+elitismo; ")
			# Torneio
			functions.plot_aptitudes(best_aptitude_valuesC,mean_aptitude_valuesC,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/exec50Torneio.pdf",option="torneio; ")
			# Torneio + Elitismo
			functions.plot_aptitudes(best_aptitude_valuesD,mean_aptitude_valuesD,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/exec50Torneio+Elitismo.pdf",option="torneio+elitismo; ")
	
		# Estatisticas 
		# (média de aptidão do melhor indivíduo por geração, média das médias de aptĩdão obtidas nas 100 gerações, número médio de 
		# gerações para encontrar o indivíduo mais apto, tempo de execução em média por chamada do algoritmo)
		# Roleta
		best_aptitude_meanA, mean_aptitude_meanA, num_geral_totalA, exec_timeA = functions.append_values(best_aptitude_meanA,mean_aptitude_meanA,best_aptitude_valuesA,mean_aptitude_valuesA,num_ger_totalA,num_generationsA,exec_timeA,startA,endA)
		# Roleta + Elitismo]
		best_aptitude_meanB, mean_aptitude_meanB, num_geral_totalB, exec_timeB = functions.append_values(best_aptitude_meanB,mean_aptitude_meanB,best_aptitude_valuesB,mean_aptitude_valuesB,num_ger_totalB,num_generationsB,exec_timeB,startB,endB)
		# Torneio
		best_aptitude_meanC, mean_aptitude_meanC, num_geral_totalC, exec_timeC = functions.append_values(best_aptitude_meanC,mean_aptitude_meanC,best_aptitude_valuesC,mean_aptitude_valuesC,num_ger_totalC,num_generationsC,exec_timeC,startC,endC)
		# Torneio + Elitismo
		best_aptitude_meanD, mean_aptitude_meanD, num_geral_totalD, exec_timeD = functions.append_values(best_aptitude_meanD,mean_aptitude_meanD,best_aptitude_valuesD,mean_aptitude_valuesD,num_ger_totalD,num_generationsD,exec_timeD,startD,endD)

	# Estatiticas Roleta
	# Escreve informações no arquivo em 'file_path'
	functions.write_statistics(mean_aptitude_meanA,mean_aptitude_meanB,num_ger_totalA,num_ger_totalB,exec_timeA,exec_timeB,file_path="Resultados Experimentais/Ex1/estatisticasRoleta.txt",option="Roleta")

	functions.plot_aptitudes_over_time(num_ger_totalA,num_ger_totalB,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/geracoesPorExecucaoRoleta.pdf",option="Seleção por Roleta")
	functions.plot_execution_time_over_time(exec_timeA,exec_timeB,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/tempoPorExecucaoRoleta.pdf",option="Seleção por Roleta")
	
	# Estatisticas Torneio
	# Escreve informações no arquivo em 'file_path'
	functions.write_statistics(mean_aptitude_meanC,mean_aptitude_meanD,num_ger_totalC,num_ger_totalD,exec_timeC,exec_timeD,file_path="Resultados Experimentais/Ex1/estatisticasTorneio.txt",option="Torneio")

	functions.plot_aptitudes_over_time(num_ger_totalC,num_ger_totalD,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/geracoesPorExecucaoTorneio.pdf",option="Seleção por Torneio")
	functions.plot_execution_time_over_time(exec_timeC,exec_timeD,population_size,PC,pm,file_path="Resultados Experimentais/Ex1/tempoPorExecucaoTorneio.pdf",option="Seleção por Torneio")

	end = time.time()
	print("execution time:", round(end-start,4),"seconds.")


if __name__ == "__main__":
	main()
