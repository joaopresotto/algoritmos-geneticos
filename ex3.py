# João Gabriel Camacho Presotto
# Trabalho 1 - Computação Inspirada pela Natureza
# Exercício 3

import functions 
import numpy as np
import time
import random
import math
import time
from progressbar import ProgressBar

# *********************************************************
# Parâmetros
# *********************************************************
population_size = 70
PC = 0.75 # taxa de crossover
pm = 0.18 # probabilidade de mutação
l = 20 # tamanho de cada individuo (x ou y) 	
target_x = 1 # Valor de x alvo 
target_y = 1 # Valor de y alvo 

# *********************************************************
# Funções
# *********************************************************

# f(x, y) = (1 − x)^2 + 100(y − x^2)^2
# tem mínimo global f(x,y) = 0 em x = 1 e y = 1
def funct(x,y):
	return ((1-x) ** 2) + (100*(y-x**2)**2)

# Retorna o bit que vai ser concatenado na bitstring de value
# '0' se value é negativo
# '1' se value é positivo
def signal_string(value):
	if value < 0:
		signal = '0'
	else: # value >= 0
		signal = '1'

	return signal

# Retorna -1 ou 1 dependendo do bit de sinal
def signal_int(bit):
	if bit == '0':
		signal = -1
	else: # bit == '1'
		signal = 1

	return signal

# Converte a bitstring value_bitstring para um valor no intervalo [-5;5]
def convert_from_bitstring(value_bitstring):
	value = value_bitstring

	# Converte para string
	value_string = np.array2string(value,separator='')
	value_string = value_string.lstrip('[').rstrip(']')

	# Retirando o bit de sinal de x e y
	signal = signal_int(value_string[0])
	
	# Pega o bitstring correspondente a x e y
	value_int = value_string[1:]

	# Converte os bitstrings de x e y para inteiro
	value_int = int(value_int,2) * signal

	# Limita x e y ao intervalo [-500,500]
	value_int = corte(value_int)

	value_final = float(value_int/100)

	return value_final

# Estratégia de corte (limita valores para intervalo [-500, 500])
def corte(n, minn=-500, maxn=500): 
	return min(max(n, minn), maxn)

def aptitude_usingFunctionValue(population):
	pop_size = population.shape[0]

	# Função de Aptidão
	f = np.zeros(shape=(pop_size),dtype=float)

	for i in range(pop_size):
		# Pega a parte da bistring i que equivale a x e a y
		value_x = convert_from_bitstring(population[i,:10])
		value_y = convert_from_bitstring(population[i,10:])

		# f = (1/(f(x_atual,y_atual) + 1)) * 10
		# min em f(x,y) = 0 f = (1/0+1)*10 => f = 10
		# Caso f tenha um valor > 0, f < 10
		# se f = 10, max aptidao
		f[i] = round((1/(funct(value_x,value_y)+1)) * 10,4)

	return f

# ************************************************************ #
#					   Algoritmo Genético 				   	   #
# ************************************************************ #	
# Parametros:
# PC = taxa de crossover [0, 1.0]
# pm = taxa de mutação [0, 1.0]
# target = bitstring do individuo alvo
# population = população que vai passar pelo algoritmo
# elistism = True ou False 
# selection = 'roulette' ou 'tournament'
# ************************************************************ #
def genetic_algorithm(PC, pm, target, population, elitism, selection):
	mean_aptitude_values = []
	best_aptitude_values = []
	optimal_individual = False
	num_iter = 0

	pop_size = population.shape[0]
	ind_size = population.shape[1]

	while not optimal_individual:	
		# Calculando aptidão (f) da população
		f = aptitude_usingFunctionValue(population)

		if elitism:
			# Encontra o melhor individuo da geração atual
			best_individual_index = np.argmax(f)
			# Salva o melhor indivíduo
			best_individual = population[best_individual_index,]	

		if selection == "roulette":
			# Seleção por roleta dos indivíduos
			P = functions.selectNewPopulationByRoulette(f,population)
		elif selection == "tournament":
			# Seleção por torneio dos individuos
			P = functions.selectNewPopulationByTournament(f,population)
		
		# Reprodução
		P = functions.reproduction(P,PC,pm,ind_size)

		if elitism:
			# Adiciona o melhor individuo que havia sido salvo na última posição da população
			P[pop_size-1,] = best_individual

		# Avaliação
		new_f = aptitude_usingFunctionValue(P)
		
		if mean_aptitude_values == []:
			# Salva a aptidão do melhor indivíduo e a media da População inicial
			best_aptitude_values.append(f[np.argmax(f)])
			mean_aptitude_values.append(round(f.sum()/pop_size))
		
		# Salva a aptidão do melhor indivíduo e a media dos individuos
		best_aptitude_values.append(new_f[np.argmax(new_f)])
		mean_aptitude_values.append(round(new_f.sum()/pop_size))

		population = P

		# se um individuo atingiu a aptidao maxima, vai sair do laço
		if 10.0 in new_f:
			optimal_individual=True

		num_iter += 1

	#print("melhor",P[np.argmax(new_f)], "- aptidão:",new_f[np.argmax(new_f)])

	mean_aptitude_values = np.array(mean_aptitude_values)
	best_aptitude_values = np.array(best_aptitude_values)

	# Separando P_x e P_y de P
	#for i in range(pop_size):
	#	for j in range(ind_size):
	#		if j < 10:
	#			population_x[i,j] = population[i,j]
	#		else:
	#			population_y[i,j-10] = population[i,j]

	return population, mean_aptitude_values, best_aptitude_values, num_iter, num_iter+1


# *********************************************************
def main():
	start = time.time()

	global target_x
	global target_y
	
	# Convertendo os valores alvos para bistring	
	target_x = corte(target_x*100) 	# Multiplicando por 100 para poder converter para bitstring
	target_x = signal_string(target_x) + format(int(abs(target_x)),'09b') # Convertendo (target*100) para bitstring
	target_x = np.array(list(target_x), dtype=int)

	target_y = corte(target_y*100) 	# Multiplicando por 100 para poder converter para bitstring
	target_y = signal_string(target_y) + format(int(abs(target_y)),'09b') # Convertendo (target*100) para bitstring
	target_y = np.array(list(target_y), dtype=int)

	# target = [target_x;target_y]
	target = np.concatenate((target_x,target_y),axis=None)

	# Variaveis para gerar os gráficos
	exec_timeA,exec_timeB, exec_timeC, exec_timeD = ([] for i in range(4))
	num_ger_totalA,	num_ger_totalB, num_ger_totalC, num_ger_totalD = ([] for i in range(4))
	best_aptitude_meanA, best_aptitude_meanB, best_aptitude_meanC, best_aptitude_meanD = ([] for i in range(4))
	mean_aptitude_meanA, mean_aptitude_meanB, mean_aptitude_meanC, mean_aptitude_meanD = ([] for i in range(4))

	p_bar = ProgressBar()

	for i in p_bar(range(100)):	
		# Inicializando população com indivíduos de tamanho 20 com valores aleatórios 0 ou 1
		init = np.random.randint(2, size=population_size*l)
		population = np.reshape(init, (population_size,l)) # matriz population_size x l

		# Algoritmos geneticos
		# Roleta
		startA = time.time()
		populationA, mean_aptitude_valuesA, best_aptitude_valuesA, num_iterA, num_generationsA = genetic_algorithm(PC,pm,target,population,elitism=False,selection="roulette")
		endA = time.time()

		# Roleta + Elitismo
		startB = time.time()
		populationB, mean_aptitude_valuesB, best_aptitude_valuesB, num_iterB, num_generationsB = genetic_algorithm(PC,pm,target,population,elitism=True,selection="roulette")
		endB = time.time()

		# Torneio
		startC = time.time()
		populationC, mean_aptitude_valuesC, best_aptitude_valuesC, num_iterC, num_generationsC = genetic_algorithm(PC,pm,target,population,elitism=False,selection="tournament")
		endC = time.time()

		#Torneio + Elitismo
		startD = time.time()
		populationD, mean_aptitude_valuesD, best_aptitude_valuesD, num_iterD, num_generationsD = genetic_algorithm(PC,pm,target,population,elitism=True,selection="tournament")
		endD = time.time()

		# Gráficos da Execução 50
		if i == 50:
			#Roleta
			functions.plot_aptitudes(best_aptitude_valuesA,mean_aptitude_valuesA,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/exec50Roleta.pdf",option="roleta; ")
			# Roleta + Elitismo
			functions.plot_aptitudes(best_aptitude_valuesB,mean_aptitude_valuesB,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/exec50Roleta+Elitismo.pdf",option="roleta+elitismo; ")
			# Torneio
			functions.plot_aptitudes(best_aptitude_valuesC,mean_aptitude_valuesC,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/exec50Torneio.pdf",option="torneio; ")
			# Torneio + Elitismo
			functions.plot_aptitudes(best_aptitude_valuesD,mean_aptitude_valuesD,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/exec50Torneio+Elitismo.pdf",option="torneio+elitismo; ")
	
		# Estatisticas 
		# (média de aptidão do melhor indivíduo por geração, média das médias de aptĩdão obtidas nas 100 gerações, número médio de 
		# gerações para encontrar o indivíduo mais apto, tempo de execução em média por chamada do algoritmo)
		# Roleta
		best_aptitude_meanA, mean_aptitude_meanA, num_geral_totalA, exec_timeA = functions.append_values(best_aptitude_meanA,mean_aptitude_meanA,best_aptitude_valuesA,mean_aptitude_valuesA,num_ger_totalA,num_generationsA,exec_timeA,startA,endA)
		# Roleta + Elitismo]
		best_aptitude_meanB, mean_aptitude_meanB, num_geral_totalB, exec_timeB = functions.append_values(best_aptitude_meanB,mean_aptitude_meanB,best_aptitude_valuesB,mean_aptitude_valuesB,num_ger_totalB,num_generationsB,exec_timeB,startB,endB)
		# Torneio
		best_aptitude_meanC, mean_aptitude_meanC, num_geral_totalC, exec_timeC = functions.append_values(best_aptitude_meanC,mean_aptitude_meanC,best_aptitude_valuesC,mean_aptitude_valuesC,num_ger_totalC,num_generationsC,exec_timeC,startC,endC)
		# Torneio + Elitismo
		best_aptitude_meanD, mean_aptitude_meanD, num_geral_totalD, exec_timeD = functions.append_values(best_aptitude_meanD,mean_aptitude_meanD,best_aptitude_valuesD,mean_aptitude_valuesD,num_ger_totalD,num_generationsD,exec_timeD,startD,endD)

	# Estatiticas Roleta
	# Escreve informações no arquivo em 'file_path'
	functions.write_statistics(mean_aptitude_meanA,mean_aptitude_meanB,num_ger_totalA,num_ger_totalB,exec_timeA,exec_timeB,file_path="Resultados Experimentais/Ex3/estatisticasRoleta.txt",option="Roleta")

	functions.plot_aptitudes_over_time(num_ger_totalA,num_ger_totalB,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/geracoesPorExecucaoRoleta.pdf",option="Seleção por Roleta")
	functions.plot_execution_time_over_time(exec_timeA,exec_timeB,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/tempoPorExecucaoRoleta.pdf",option="Seleção por Roleta")
	
	# Estatisticas Torneio
	# Escreve informações no arquivo em 'file_path'
	functions.write_statistics(mean_aptitude_meanC,mean_aptitude_meanD,num_ger_totalC,num_ger_totalD,exec_timeC,exec_timeD,file_path="Resultados Experimentais/Ex3/estatisticasTorneio.txt",option="Torneio")

	functions.plot_aptitudes_over_time(num_ger_totalC,num_ger_totalD,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/geracoesPorExecucaoTorneio.pdf",option="Seleção por Torneio")
	functions.plot_execution_time_over_time(exec_timeC,exec_timeD,population_size,PC,pm,file_path="Resultados Experimentais/Ex3/tempoPorExecucaoTorneio.pdf",option="Seleção por Torneio")

	end = time.time()
	print("execution time:", round(end-start,4),"seconds.")

if __name__ == "__main__":
	main()