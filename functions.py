# João Gabriel Camacho Presotto
# Trabalho 1 - Computação Inspirada pela Natureza
# Funções utilizadas em todos os exercícios

import numpy as np
import random
import statistics
import os
from matplotlib import pyplot as plt

# *********************************************************
# Funções para funcionamento do algoritmo genético
# *********************************************************

# Função para crossover da posição cp até o final (l) do indivíduo i com i+1
def crossover(P, i, cp, l):
	tmp = P[i+1,cp:l].copy()
	P[i+1,cp:l], P[i,cp:l] = P[i,cp:l], tmp

	return P

def reproduction(P, PC, pm, l):
	pop_size = P.shape[0]

	# Crossover
	for i in range(0,pop_size,2):
		r = round(random.random(),2)
		if r <= PC:
			# Define o ponto de crossover
			cp = random.randint(1,l-1)
			if i != pop_size-1:
				P = crossover(P,i,cp,l)
			else: # caso o tamanho da população seja impar e i não consiga fazer crossover com i+1, fazemos crossover de i e i-1
				P = crossover(P,i-1,cp,l)
	# Mutação
	for i in range(pop_size):
		for j in range(l):
			r = round(random.random(),2)
			if r <= pm:
				#print("Executando mutação em: [", i,",",j,"]")
				P[i,j] = 1 - P[i,j]		
	return P


def hammingDistance(target,population):
	pop_size = population.shape[0]
	# Conta quantos elementos diferentes existem entre o valor alvo e o individuo i da população (np.count_nonzero(target!=population[i,]), armazenando em h	
	h = np.array([np.count_nonzero(target!=population[i,]) for i in range(pop_size)])
	return h

# f[i] = l - hammingDistance[i]
def aptitude(l,hammingDist):
	return np.array([(l - hammingDist[i]) for i in range(hammingDist.size)])

# Se um número aleatório entre 0 e 360 está no intervalo de 
# determinado indivíduo, retorna o indice i desse indivíduo
def roulette(fL):
	aux = 0
	s = 0
	r = round(random.uniform(0,360),4)
	
	for i in range(fL.size):
		s += fL[i]
		if r <= s:
			aux = i
			break

	return aux

# Seleção por Roleta
def selectNewPopulationByRoulette(f, population):
	pop_size = population.shape[0]

	# Determinando porção da roleta de cada indivíduo
	fL = np.array([round((360*f[i])/f.sum(),4) for i in range(pop_size)])

	# Gerando nova população P a partir da roleta
	P = np.array([population[roulette(fL)] for i in range(pop_size)])

	return P

# Seleção por torneio (três possíveis candidatos)
def selectNewPopulationByTournament(f,population, number_of_candidates=3):
	pop_size = population.shape[0]

	# Determinando porção da roleta de cada indivíduo
	fL = np.array([round((360*f[i])/f.sum(),4) for i in range(pop_size)])

	# Estrutura para armazenar o indice dos três possíveis candidatos
	candidates = np.zeros(shape=(pop_size,number_of_candidates), dtype=int)

	# Preenche estrutura de torneio com três possiveis candidatos em cada linha
	for i in range(pop_size):
		for j in range(number_of_candidates):
			candidates[i][j] = roulette(fL)

	# Seleciona o melhor candidato de cada linha
	best_canditates = np.amax(candidates,axis=1)

	# Gerando nova população P a partir de best_candidates
	P = np.array([population[best_canditates[i]] for i in range(pop_size)])

	return P

# *********************************************************
# Funções para gerar gráficos e arquivos com estatísticas
# *********************************************************
def plot_aptitudes(best_aptitude_values,mean_aptitude_values,population_size,PC,pm,file_path,option):
	f1 = plt.figure()
	plt.plot(best_aptitude_values,label='Melhor aptidão')
	plt.plot(mean_aptitude_values,label='Aptidão média')
	plt.title("Aptidão dos indivíduos em cada geração.\n" + r"($\bf{seleção=}$" + option + r"$\bf{tam\_população=}$" + str(population_size) + "; " + r"$\bf{pc=}$" + str(PC) + 
	"; " + r"$\bf{pm=}$" + str(pm) + ")")
	plt.xlabel("Geração")
	plt.ylabel("Aptidão")
	plt.legend()
	f1.savefig(str(file_path),bbox_inches='tight')

def plot_aptitudes_over_time(num_ger_totalA,num_ger_totalB,population_size,PC,pm,file_path,option):
	f1 = plt.figure()
	plt.plot(num_ger_totalA,label=str(option))
	plt.plot(num_ger_totalB,label=str(option)+'+Elitismo')
	plt.title("Quantidade necessária de gerações para obtenção de indivíduo mais apto por execução.\n" + r"($\bf{tam\_população=}$" + str(population_size) + "; " + r"$\bf{pc=}$" + str(PC) + 
	"; " + r"$\bf{pm=}$" + str(pm) + ")",fontsize=10)
	plt.xlabel("Número de Execuções")
	plt.ylabel("Gerações")
	plt.legend()
	f1.savefig(str(file_path),bbox_inches='tight')

def plot_execution_time_over_time(exec_timeA,exec_timeB,population_size,PC,pm,file_path,option):
	f2 = plt.figure()
	plt.plot(exec_timeA,label=str(option))
	plt.plot(exec_timeB,label=str(option)+'Elitismo')
	plt.title("Tempo necessário para encontrar o indivíduo mais apto por execução.\n" + r"($\bf{tam\_população=}$" + str(population_size) + "; " + r"$\bf{pc=}$" + str(PC) + 
	"; " + r"$\bf{pm=}$" + str(pm) + ")",fontsize=10)
	plt.xlabel("Numero de Execuções")
	plt.ylabel("Tempo de Execução (s)")
	plt.legend()
	f2.savefig(str(file_path),bbox_inches='tight')

def write_statistics(mean_of_mean_aptitudeA,mean_of_mean_aptitudeB,mean_generationsA,mean_generationsB,exec_timeA,exec_timeB,file_path,option):
	file = open(str(file_path),'w+')
	file.write(str(option)+":\n")
	file.write("Média das médias de aptidão por geração obtida: %.2f" % round(statistics.mean(mean_of_mean_aptitudeA),2) + "\n")
	file.write("Desvio padrão das médias de médias de aptidão: %.2f" % round(statistics.stdev(mean_of_mean_aptitudeA),2) + "\n")
	file.write("Numero médio de gerações para atingir o individuo mais apto: %.2f" % round(statistics.mean(mean_generationsA),2) + "\n")
	file.write("Desvio padrão do número medio de gerações: %.2f" % round(statistics.stdev(mean_generationsA),2) + "\n")
	file.write("Tempo médio de execução por geração: %.2f" % round(statistics.mean(exec_timeA),2) + " segundos\n")

	file.write("****************************************************************" + "\n")
	
	file.write(str(option)+"+Elitismo:\n")
	file.write("Média das médias de aptidão por geração obtida: %.2f" % round(statistics.mean(mean_of_mean_aptitudeB),2) + "\n")
	file.write("Desvio padrão das médias de médias de aptidão: %.2f" % round(statistics.stdev(mean_of_mean_aptitudeA),2) + "\n")
	file.write("Numero médio de gerações para atingir o individuo mais apto: %.2f" % round(statistics.mean(mean_generationsB),2) + "\n")
	file.write("Desvio padrão do número medio de gerações: %.2f" % round(statistics.stdev(mean_generationsB),2) + "\n")
	file.write("Tempo médio de execução por geração: %.2f" % round(statistics.mean(exec_timeB),2) + " segundos\n")
	file.close()

def append_values(best_aptitude_mean,mean_of_mean_aptitude,best_aptitude_values,mean_aptitude_values,num_gen_total,num_generations,exec_time,start,end):
	best_aptitude_mean.append(statistics.mean(best_aptitude_values))
	mean_of_mean_aptitude.append(statistics.mean(mean_aptitude_values))
	num_gen_total.append(num_generations)
	exec_time.append(end-start)

	return best_aptitude_mean, mean_of_mean_aptitude, num_gen_total, exec_time